import _osx_support
from datetime import datetime, timedelta
import parser
# main map for incoming tweets
MAIN_MAP = []


class dead_site:

    def __init__(self, fname, birthdate, from_where):
        self.full_name = fname
        self.birthdate = datetime.strptime(birthdate, '%a %b %d %X %z %Y')  # time of post via twitter, current post in question
        self.from_where = from_where
        self.threshold = timedelta()
        self.count = 0
        self.age_array = []

    def __eq__(self, other):
        """Override the default Equals behavior"""
        this_size = None
        similarity = 0
        if self.website().lower() == other.website().lower():
            return True
        else:
            if len(self.website()) > len(other.website()):
                this_size = len(self.website())
            # if the names are NOT exactly the same how confident are you that current tweet is related to other tweets
            else :
                this_size = len(other.website())
            for char in self.website().lower():
                if char in other.website().lower():
                    similarity += 1
            return (similarity*100/this_size) > 70

    def age(self):
        b_date = self.birthdate
        # sets the timezone of bot to match the tweet to help better calculate if URL is down
        age = datetime.now(b_date.tzinfo)
        # adds the birth to the age array
        self.age_array.append(age)
        print(age)
        self.count += 1
        return age

    def website(self):  # gathers the website title
        ret = ""
        for p in self.full_name.split(" "):
                if '@' not in p:
                    if '#' not in p:
                        if 'RT' not in p:
                            ret = ret + p
        return ret

    def age_range(self):
        if len(self.age_array) is 0:
            return self.age()  # returns the very first occurrence of the time at w
        else:
            return self.age_array[len(self.age_array)-1] - self.age_array[0]

