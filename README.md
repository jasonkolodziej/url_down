# URL_down
This is a personal project helping another company to detect if their app/ website is down to their clients.
This script is currently looks for the '#URLDown' on incoming public twitter stream and solves whether or not the
there is "Complaints" towards a particular site. Then app makes a post to the account in which it is related to. 

The twitter account used for this account is '@URL_Down', which has a public profile. Tweeting examples such as...

"generalmotors.com #URLDown"
 
 or misspelled 
 
 "genralmotor.com #URLDown" ...
 
 the bot will compare the similarity with the
site URLs and have the ability to know that people are talking about the same site. Currently the bot will post 
after two similar tweets, due to testing purposes. However, the threshold values of similarity in site names 
and the confidence level of when to make an announcement can all be changed. This is just early stages of the bot
but owns the full potential of AI with the right implementation.


Copyright © 2018 by Jason A. Kolodziej
All rights reserved.
