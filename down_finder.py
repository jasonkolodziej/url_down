from tweepy.streaming import StreamListener, Stream
from gateway import Post_Back
import json
from mapper import MAIN_MAP
from mapper import dead_site





# set up stream listener
class listener(StreamListener):

    def on_data(self, data):
        all_data = json.loads(data)
        # collect all desired data fields
        if 'text' in all_data:
            tweet = all_data["text"]
            if '#URLDown' in tweet:  # this ensure the proper # is used and we do not collect dummy tweets
                created_at = all_data["created_at"]
                retweeted = all_data["retweeted"]
                username = all_data["user"]["screen_name"]
                user_tz = all_data["user"]["time_zone"]
                user_location = all_data["user"]["location"]
                user_coordinates = all_data["coordinates"]

                # if coordinates are not present store blank value
                # otherwise get the coordinates.coordinates value
                if user_coordinates is None:
                    final_coordinates = user_coordinates
                else:
                    final_coordinates = str(all_data["coordinates"]["coordinates"])

                print((username, tweet, user_location, user_coordinates, user_tz, created_at))

                # create a query that might be a site down
                site = dead_site(tweet,created_at,user_location)

                # checks to see if the site has already been talked about before
                if site in MAIN_MAP:
                        current_site = MAIN_MAP[MAIN_MAP.index(site)]
                        current_site.birthdate = site.birthdate
                        current_site.age()
                        print((current_site.age_range()/current_site.count))
                        # check the RELATIVE FREQUENCY and see whether or not BOT should post
                        if (current_site.age_range()/current_site.count) > current_site.threshold and\
                                current_site.count is not 1:
                                Post_Back.update_status("As of " + str(current_site.age_array[current_site.count-1])
                                                        + ", website \"" + current_site.website() + "\" is down")


                # push to the map if not
                else:
                    site.age()
                    MAIN_MAP.append(site)

                return True
        else:
            return True

    def on_error(self, status):
        print(status)
