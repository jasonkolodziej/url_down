import sys
import down_finder
from gateway import auth


# for tweet in tweepy.Cursor(api.search, q='#URLDown').items():
#     try:
#         print("Found tweet by: @" + tweet.user.screen_name)
#     except tweepy.TweepError as e:
#         print(e.reason)
#         sleep(10)
#         continue
#     except StopIteration:
#         break



# create stream and filter on a searchterm
twitterStream = down_finder.Stream(auth, down_finder.listener())
twitterStream.filter(track=["#URLDown"], stall_warnings=True)

