import _osx_support
import tweepy

# Consumer keys and access tokens, used for OAuth

consumer_token = '###########################'
consumer_secret = '###################'
access_token = '########################################'
access_token_secret = '##############################################'

# callback_url = 'www.twitter.com'

# OAuth process, using the keys and tokens
auth = tweepy.OAuthHandler(consumer_token, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
auth.secure = True
# Creation of the actual interface, using authentication
Post_Back = tweepy.API(auth)
